#!/bin/sh

# Fetch backup data over the network
if [ "${BACKUP_HOST}" != "false" ]; then
	SSH_KEY_FILE_BACKUP="/tmp/key.backup"
	echo "${BACKUP_SSHKEY}" > ${SSH_KEY_FILE_BACKUP}
	chmod 600 ${SSH_KEY_FILE_BACKUP}
        rsync -arczv -e "ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${SSH_KEY_FILE_BACKUP}" /source/ ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_PATH}/
        touch /source/LATESTBACKUP
    rm ${SSH_KEY_FILE_BACKUP}
	echo "Synchronized backup data over the network from ${BACKUP_HOST}"
fi